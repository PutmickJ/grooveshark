// COP 3503 Fall 2012 Project 2
// COP 3503 Patrick Scanlan Project 4

import java.util.Arrays;
import java.util.ArrayList;

public class RedBox {
    /*******************
     * PROPERTIES
     *******************/

    private Videogame[] videogames;
    private Movie[] movies;

    private ArrayList<Customer> customers;

    /*******************
     * CONSTRUCTOR(S)
     *******************/

    public RedBox(Videogame[] vg, Movie[] m) {
        this.videogames = vg;
        this.movies = m;
        customers = new ArrayList<Customer>();
    }


    /*******************
     * Customer add
     *******************/
    public void addCustomer(Customer customer) {
        this.customers.add(customer);
    }

    /*******************
     * SEARCHING METHODS
     *******************/
    public int findVideogameByTitle(String title) {
        for(int i = 0; i < videogames.length; i++) {
            if (videogames[i].getTitle().equals(title))
                return i;
        }

        return -1;
    }

    public int findMovieByTitle(String title) {
        for(int i = 0; i < movies.length; i++) {
            if (movies[i].getTitle().equals(title))
                return i;
        }

        return -1;
    }

    // Returns the videogame at a certain index
    public Videogame getVideogameByIndex(int index) {
        if (index < videogames.length && index >= 0)
            return videogames[index];
        else
            return null;
    }

    // Returns the movie at a certain index
    public Movie getMovieByIndex(int index) {
        if (index < movies.length && index >= 0)
            return movies[index];
        else
            return null;
    }

    /*******************
     * SORTING METHODS
     *******************/

    /*************
      For Videogames:
     *************/
    public void sortVideogamesByPopularity() {
        Arrays.sort(videogames, new Videogame());
    }

    public void sortVideogamesByTitle() {
        Arrays.sort(videogames);
    }

    public void sortVideogamesByRating() {
        Arrays.sort(videogames, new RatingComparison());
    }

    public void sortVideogamesByPlatform() {
        Arrays.sort(videogames, new VideogameComparison());
    }

    // TODO:
    public void sortVideogamesByReleaseDate() {
        Arrays.sort(videogames, new DateComparison());
    }

    /*************
      For Movies:
     *************/

    public void sortMoviesByPopularity() {
        Arrays.sort(movies, new Movie());
    }

    public void sortMoviesByTitle() {
        Arrays.sort(movies);
    }

    public void sortMoviesByRating() {
        Arrays.sort(movies, new RatingComparison());
    }

    public void sortMoviesByFormat() {
        Arrays.sort(movies, new MovieComparison());
    }

    // TODO:
    public void sortMoviesByReleaseDate() {
        Arrays.sort(movies, new DateComparison());
    }

    /*******************
     * Get/set Methods
     *******************/
    public Videogame[] getVideogames() {
        return videogames;
    }

    public void setVideogames(Videogame[] videogames) {
        this.videogames = videogames;
    }

    public Movie[] getMovies() {
        return movies;
    }

    public void setMovies(Movie[] movies) {
        this.movies = movies;
    }

    public ArrayList<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(ArrayList<Customer> customers) {
        this.customers = customers;
    }

    /*******************
     * Print methods
     *******************/
    public void printVideogames() {
        for (int i = 0; i < videogames.length; i++) {
            System.out.println((i + 1) + ". " + videogames[i]); //videogames[i].getTitle());
            /*if (i < videogames.length - 1)
                System.out.print("\n");*/
        }
    }

    public void printMovies() {
        for (int i = 0; i < movies.length; i++) {
            System.out.println((i + 1) + ". " + movies[i]); //.getTitle());
            /*if (i < movies.length - 1)
                System.out.print("\n");*/
        }
    }
    
    public ArrayList<Integer> searchVideogamesInRange (double min, double max){
    	int numComparisons = 0;
    	int k = 0;
    	ArrayList<Integer> a1 = new ArrayList<Integer>();
    	for (int i = 0; i < videogames.length; i++){
    		if (videogames[i].getAverageRating() >= min){
    			if (videogames[i].getAverageRating() <= max){
    				a1.add(i);
    				numComparisons++;
    			}
    			else {
    				numComparisons++;
    			}
    		}
    		else{
    			numComparisons++;
    		}
    	}
    	
    	a1.add(numComparisons);
    	return a1;
    }
    
    public ArrayList<Integer> searchVideogamesInRangeSorted (double min, double max){
    	int numComparisons = 0;
    	int left = 0;
    	int right = videogames.length;
    	int count1 = 0;
    	int count2 = 0;
    	ArrayList<Integer> a1 = new ArrayList<Integer>();
    	int middle = (left + right) / 2;
    	int middle2 = middle;
    	while (left < right){
    		middle = (left + right) / 2;
    		if (videogames[middle].getAverageRating() > max){
    			right--;
    			numComparisons++;
    		}
    		if (videogames[middle].getAverageRating() < min){
    			left++;
    			numComparisons++;
    		}
    		else{
    			while (videogames[middle].getAverageRating() < max){
    				middle--;
    				count2++;
    				numComparisons++;
    				if (middle == 0){
    					numComparisons++;
    					break;
    				}
    			}
    			break;
    		}
    	}
    	for (int j = 0; j < count2; j++){
    		a1.add(middle + j);
    	}
    	
    	while (left < right){
    		
    		middle = (left + right) / 2;
    		if (videogames[middle].getAverageRating() > max){
    			right--;
    			numComparisons++;
    		}
    		if (videogames[middle].getAverageRating() < min){
    			left++;
    			numComparisons++;
    		}
    		else{
    			while (videogames[middle].getAverageRating() > min){
    				middle++;
    				count1++;
    				numComparisons++;
    				if (middle >= videogames.length){
    					numComparisons++;
    					break;
    				}
    			}
    			break;
    		}
    		
    	}
    	for (int i = count1; i > 0; i--){
    		a1.add(middle - i);
    	}
    	
    	a1.add(numComparisons);
    	return a1;
    }
    
    public int insertionSort(boolean movieCheck){
    	int j;
    	int numComparisons = 0;
    	if (movieCheck == true){
    		for (int i = 1; i < movies.length; i++){
    			double temp = movies[i].getAverageRating();
    			Movie tempMovie = movies[i];
    			j = i;
    			while ((j > 0) && (movies[j-1].getAverageRating() < temp)){
    				movies[j] = movies[j-1];
    				j--;
    				numComparisons++;
    			}
    			movies[j] = tempMovie;
    			numComparisons++;
    		}
    	}
    	else {
    		int l;
    		for (int k = 1; k < videogames.length; k++){
    			double temp = videogames[k].getAverageRating();
    			Videogame tempGame = videogames[k];
    			l = k;
    			while ((l > 0) && (videogames[l-1].getAverageRating() < temp)){
    				
    				videogames[l] = videogames[l-1];
    				l--;
    				numComparisons++;
    			}
    			videogames[l] = tempGame;
    			numComparisons++;
    		}
    	}
    	return numComparisons;
    }
    
    public Rental[] merge(){
    	Rental[] array1 = new Rental[movies.length + videogames.length];
    	int i = 0;
    	int j = 0;
    	int k = 0;
    	while (i < movies.length && j < videogames.length){
    		if (movies[i].getAverageRating() < videogames[j].getAverageRating()){
    			array1[k] = videogames[j];
    			j++;
    			
    		}
    		else if (movies[i].getAverageRating() > videogames[j].getAverageRating()){
    			array1[k] = movies[i];
    			i++;
    			
    		}
    		else {
    			Rental rental1 = movies[i];
    			Rental rental2 = videogames[j];
    			if (rental1.compareTo(rental2) < 0){
    				array1[k] = movies[i];
    				i++;
    			}
    			else if (rental1.compareTo(rental2) > 0){
    				array1[k] = videogames[j];
    				j++;
    			}
    			else{
    				array1[k] = videogames[j];
    				j++;
    			}
    		}
    		k++;
    	}
    	
    	while (i < movies.length){
    		array1[k] = movies[i];
    		i++;
    		k++;
    	}
    	
    	while (j < videogames.length){
    		array1[k] = videogames[j];
    		j++;
    		k++;
    	}
    	return array1;
    }

}
